# Cloud Computing in General
<!-- Updated 9 May 2023 -->
<!-- Cloud Computing - Is it not just some resources in someone else's data center? -->

The views expressed on this blog are my own and do not necessarily reflect the views of Oracle.

Cloud Computing should deliver the following functionality and capabilities:

<img alt="Cloud Computing" src="images/cloud-computing.png" title="Cloud Computing" width="70%" style="float:right">

Cloud provider capabilities [by CNCF](https://landscape.cncf.io/) shows some cloud capabilities by Vendor.

# Oracle Cloud Infrastructure (OCI)
Why choose Oracle Cloud for Oracle and non-Oracle workloads?
There are [6 key reasons](https://www.oracle.com/cloud/why-oci/) why customers choose to run applications and databases on Oracle Cloud Infrastructure (OCI):

1. [OCI offers superior price-performance](https://www.oracle.com/cloud/economics/)
2. [Easy to migrate critical enterprise workloads](https://www.oracle.com/cloud/why-oci/#easiertomigrate)
3. [Oracle Cloud provides the most support for hybrid cloud strategies](https://www.oracle.com/cloud/multicloud/hybrid-cloud/)
4. [Everything you need to build modern cloud native applications](https://www.oracle.com/cloud/why-oci/#cloudnative)
5. [Autonomous services automatically check, secure, tune, and scale your apps](https://www.oracle.com/cloud/why-oci/#autonomous)
6. [Cloud security is built in, and switched on by default, at no extra charge](https://www.oracle.com/cloud/why-oci/#builtinsecurity)

<hr>

## System Administration for Oracle Cloud Infrastructure (OCI) workloads
[System administration](https://gitlab.com/ms76152/system-administration) of virtual machines running on cloud computing
